package com.hunza.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hunza.model.Caterer;

/**
 * This interface represents the CRUD operations. That is all we need.
 * 
 *
 */
@Repository
public interface CatererRepository extends MongoRepository<Caterer, Long> {

	public Caterer findByName(String name);

	public List<Caterer> findAllByCity(String city);

}
