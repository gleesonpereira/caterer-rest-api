package com.hunza.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import com.hunza.model.Caterer;
import com.hunza.service.SequenceGeneratorService;

/**
 * This class is used to latch on to Mongo DB events. We assign an id to the caterer
 * before saving it and to publish a message to the broker upon caterer resource
 * being successfully stored in MongoDB.
 *
 */
@Component
public class CatererModelListener extends AbstractMongoEventListener<Caterer> {

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	@Autowired
	private CatererPublisher catererPublisher;
	
	@Override
	public void onBeforeConvert(BeforeConvertEvent<Caterer> event) {
		if (event.getSource().getId() < 1) {
            event.getSource().setId(sequenceGeneratorService.generateSequence(Caterer.SEQUENCE_NAME));
        }
	}
	
	@Override
	public void onAfterSave(AfterSaveEvent<Caterer> event) {
		catererPublisher.sendToTopic(event.getSource());
	}
}
