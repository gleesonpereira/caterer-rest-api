package com.hunza.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.hunza.model.Caterer;

/**
 * This class does the heavy lifting of sending the event message to the
 * topic.
 *
 */
@Component
public class CatererPublisher {

	private static final String TOPIC_DESTINATION = "caterer-topic";
	
	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendToTopic(Caterer caterer) {
		jmsTemplate.convertAndSend(TOPIC_DESTINATION, caterer);
	}
}
