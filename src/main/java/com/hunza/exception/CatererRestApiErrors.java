package com.hunza.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class is used to wrap the errors that are generated out of the request
 * validation. Particularly, wraps MethodArgumentNotValidException.
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CatererRestApiErrors {

	private HttpStatus status;
    private String message;
    private List<String> errors;
    
}
