package com.hunza.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/** This class is used to redirect application root url(/) to swagger documentation
 * 
 */
@Configuration
public class RestApiRootRedirecter implements WebMvcConfigurer  {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
	    registry.addViewController("/").setViewName("redirect:/swagger-ui/");
	}
}
