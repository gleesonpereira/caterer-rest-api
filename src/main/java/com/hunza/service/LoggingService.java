package com.hunza.service;

/**
 * This class represents the service class contract used for logging request/response
 * payload.
 * 
 */
public interface LoggingService {

	public void logRequest(Object body);
	
	public void logResponse(Object body);
}
