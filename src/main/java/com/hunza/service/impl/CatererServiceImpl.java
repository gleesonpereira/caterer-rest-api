package com.hunza.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.hunza.model.Caterer;
import com.hunza.repository.CatererRepository;
import com.hunza.service.CatererService;

import lombok.extern.slf4j.Slf4j;

/**
 * Service implementation class used for Caterer Rest API operations.
 *
 */
@Service
@Slf4j
public class CatererServiceImpl implements CatererService {
	
	@Autowired
	private CatererRepository repository;

	@Override
	@CachePut("caterers")
	public Caterer save(Caterer caterer) {
		log.info("Saved a new caterer with name "+caterer.getName());
		return repository.save(caterer);
	}

	@Override
	@Cacheable("caterers")
	public Optional<Caterer> findById(long id) {
		log.info("Retreieved caterer by id "+id);
		return repository.findById(id);
	}

	@Override
	@Cacheable("caterers")
	public Caterer findByName(String name) {
		log.info("Retreieved caterer by name "+name);
		return repository.findByName(name);
	}

	@Override
	@Cacheable("caterersByCity")
	public List<Caterer> findAllByCity(String city) {
		log.info("Retreieved caterer by city "+city);
		return repository.findAllByCity(city);
	}

}
