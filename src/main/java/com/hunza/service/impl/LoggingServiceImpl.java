package com.hunza.service.impl;

import org.springframework.stereotype.Service;

import com.hunza.service.LoggingService;

import lombok.extern.slf4j.Slf4j;

/**
 * Service implementation class used for logging request/response payload.
 * 
 *
 */
@Service
@Slf4j
public class LoggingServiceImpl implements LoggingService {

	@Override
	public void logRequest(Object body) {
		if(body != null) {
			log.info("Request Payload= ["+ body +" ]");
		}
	}

	@Override
	public void logResponse(Object body) {
		if(body != null) {
			log.info("Response Payload= ["+ body +" ]");
		}
	}

}
