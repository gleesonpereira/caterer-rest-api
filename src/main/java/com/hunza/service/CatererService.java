package com.hunza.service;

import java.util.List;
import java.util.Optional;

import com.hunza.model.Caterer;

/**
 * This class represents the service class contract used for Caterer Rest API operations.
 * 
 *
 */
public interface CatererService {
	
	Caterer save(Caterer caterer);
	
	Optional<Caterer> findById(long id);
	
	Caterer findByName(String name);
	
	List<Caterer> findAllByCity(String city);
}
