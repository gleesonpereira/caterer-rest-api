package com.hunza.model;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * This class represents the Caterer resource.
 * 
 *
 */
@Document(collection = "caterers")
@Getter 
@Setter 
@NoArgsConstructor
@ToString
public class Caterer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4236327994394541075L;

	@Transient
	public static final String SEQUENCE_NAME = "caterers_sequence"; 
	
	@Id
	private long id;
	
	@NotBlank(message = "Caterer name must not be null or blank")
	private String name;
	
	@NotBlank(message = "City name must not be null or blank")
	@Pattern(regexp="^([A-Za-z]*)$", message = "City name must only contain alphabets")
	private String city;
	
	@NotBlank(message = "Street name must not be null or blank")
	private String streetName;
	
	@NotBlank(message = "Street number must not be null or blank")
	private String streetNumber;
	
	private String postalCode;
	
	@Positive(message = "Minimum number of guest must be a positive number")
	private int minGuest;
	
	@Positive(message = "Maximum number of guest must be a positive number")
	private int maxGuest;
	
	private String phoneNumber;
	
	@NotBlank(message = "Mobile number must not be null or blank")
	private String mobileNumber;
	
	@NotBlank(message = "Email must not be null or blank")
	@Email(message = "Please enter a valid email. For example, your_username@domain.com")
	private String email;
	
}
