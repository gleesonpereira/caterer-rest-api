package com.hunza.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * As auto generated identity is not supported in MongoDB. This class
 * represents a database entity, that contains a sequence name and it's
 * current value.
 *
 */
@Document(collection = "db_sequences")
@Getter
@Setter
@NoArgsConstructor
public class DBSequences {

	@Id
	private String id;
	private long seq;
	
}
