package com.hunza.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.hunza.model.Caterer;
import com.hunza.service.CatererService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * This is the main controller class that contains the Rest API operations
 * on a Caterer resource.
 *
 */
@RestController
@RequestMapping("api")
@Slf4j
public class CatererController {
	
	public static final String NO_CATERER_FOUND = "No caterer found with";
	public static final String NO_CATERER_FOUND_BY_CITY = "No caterer(s) found by city";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String CORRECTION_MSG = "Please provide a valid input";

	@Autowired
	private CatererService catererService;
	
	@PostMapping(value="/caterers")
	@ApiOperation(value = "Saves a caterer.", 
		notes = "Saves a caterer to the caterer collection.")
	public Caterer save(@Valid @RequestBody Caterer caterer) {
		return catererService.save(caterer);
	}

	@GetMapping(value="/caterers/name/{name}")
	@ApiOperation(value = "Find a caterer by name.", 
		notes = "Finds a caterer by name provided as an input by the user.")
	public Caterer findByName(@Valid @PathVariable String name) {
		Caterer caterer = catererService.findByName(name);

		if(caterer != null) {
			ResponseEntity.ok();
			return caterer;
		}

		log.error(NO_CATERER_FOUND + " "+ NAME + " " + name + CORRECTION_MSG);
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NO_CATERER_FOUND + " "+ NAME + " " + name + ". " + CORRECTION_MSG);
	}

	@GetMapping(value="/caterers/id/{id}")
	@ApiOperation(value = "Find a caterer by id.", 
		notes = "Finds a caterer by id provided as an input by the user.")
	public Caterer findById(@PathVariable long id) {
		try {
			Optional<Caterer> optionalCaterer = catererService.findById(id);
			Caterer caterer  = optionalCaterer.get();
			ResponseEntity.ok();
			return caterer;
		} catch(NoSuchElementException nsee) {
			log.error(NO_CATERER_FOUND + " "+ ID + " " + id + CORRECTION_MSG);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NO_CATERER_FOUND + " "+ ID + " " + id + ". " + CORRECTION_MSG);
		}
	}

	@GetMapping(value="/caterers/city/{city}")
	@ApiOperation(value = "Find a caterer by city.", 
		notes = "Finds list of caterers by city provided as an input by the user.")
	public List<Caterer> findAllByCity(@PathVariable String city) {
		List<Caterer> listOfCaterers = catererService.findAllByCity(city);
		
		if(!listOfCaterers.isEmpty()) {
			ResponseEntity.ok();
			return listOfCaterers;
		}
		
		log.error("No caterer(s) found by city "+city);
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NO_CATERER_FOUND_BY_CITY + " " + city);
	}
}
