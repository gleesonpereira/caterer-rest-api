package com.hunza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableMongoRepositories
@EnableJms
@EnableCaching
public class CatererApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatererApiApplication.class, args);
	}
	
}
