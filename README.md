### About this project
Rest API operations to save and retrieve Caterer resource.

### System Constraints Implemented
1. Documented the swagger api model and response.
1. Log requests and responses payload.
1. Validate the request payload.
1. Implemented data cache layer to improve performance for read queries.
1. Implemented error handling.

### How to build the application
* Use the following maven command to build the application

```
mvn clean install
```

* To use the locally built image with minikube, you have to build on the docker environment attached to minikube

```
eval $(minikube -p minikube docker-env)
```

* Then use the following docker command to create a container image

```
docker build -t caterer-rest-api:0.0.1 .
```

### K8s scripts
The folder k8s-scripts contains the Kubernetes deployment script for the rest application. Please execute
the script in the following order
1. Create the config map.
1. Create the service.
1. Create the deployment.

If you are on Minikube Cluster, then the LoadBalancer will not generate an External IP. So, use the following
minikube command to start tunneling the service endpoint

```
minikube service caterer-svc
```

You can access the application using the external ip exposed by the load balancer or the one tunneled by minikube
whichever is applicable.