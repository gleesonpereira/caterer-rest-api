FROM azul/zulu-openjdk-alpine:11.0.12
LABEL maintainer="Chuck Finley chuckfinley@yahoo.com"
RUN adduser -D appusr
RUN mkdir -p /app/config
RUN chown appusr /app
USER appusr
COPY target/caterer-rest-api-0.0.1-SNAPSHOT.jar /app/
COPY src/main/resources/log4j2.xml /app/config/
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "caterer-rest-api-0.0.1-SNAPSHOT.jar"]
